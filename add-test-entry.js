'use strict';
const
	redis = require('./redis'),
	date = process.argv[2] ? new Date(process.argv[2]) : new Date,
	time = Math.random() * 60*60*2
	;

redis.logTask({
	'forceDate': date,
	time,
	'task': 'test task please ignore',
	'tags': []
})
	.then(({ task }) => {
		console.log(task);
		redis.end();
	});
