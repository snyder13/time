/**
 * Plugins add global functionality to an application, using something general (not domain- or app-specific)
 */

/**
 * Generic state-initializer
 *
 * Render the page with elements with the class name 'vuex-seed', and one or more data-KEY="JSON value" attributes to be imported into the store.
 */
const dataAttrRe = /^data-(.*)$/;
const readSeedData = (store) => {
	Array.from(document.querySelectorAll('.vuex-seed')).forEach((el) => {
		Object.keys(el.attributes).forEach((key) => {
			const ma = el.attributes[key].name.match(dataAttrRe);
			if (ma) {
				store.commit('SEED', {
					'key'  : ma[1],
					'value': JSON.parse(el.attributes[key].value)
				});
			}
		});
	});
};

export default [ readSeedData ];
