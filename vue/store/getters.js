'use strict';
/**
 * These are for calculated properties, for example if you wanted to label the count of some elements in the state matching certain criteria, you can encode that here
 */

import { getElapsedLabel } from './actions.js';

export const moreWeeksExist = (state) => {
	return state.weeks.length > state.visibleWeeks.length;
};

// group task log by day so headings are easy in the template
export const logByDay = (state) => {
	if (!state.log.length) {
		return;
	}
	const rv = [];
	let lastDate = new Date(state.log[0].date), day = [];
	const push = (newDate) => {
		rv.push({
			'heading':
				['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][lastDate.getDay()] + ', ' +
				['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][lastDate.getMonth()] + ' ' +
				lastDate.getDate(),
			'entries': day,
			'total': getElapsedLabel(day.reduce((sum, entry) => { return sum + entry.time; }, 0))
		});
		day = [];
		lastDate = newDate;
	};
	state.log.forEach(({ task, tags, date, time }) => {
		const dt = new Date(date);
		if (dt.getFullYear() !== lastDate.getFullYear() || dt.getMonth() !== lastDate.getMonth() || dt.getDate() !== lastDate.getDate()) {
			push(dt);
		}
		day.push({ task, tags, date, time, 'timeLbl': getElapsedLabel(time), dt });
	});
	if (day.length) {
		push();
	}
	return rv;
};
