'use strict';
/**
 * Actions defining reactive or visual updates to the application (onclick events and the like)
 */

// some node modules can be webpacked, if they don't use node-specific API
const uuid = require('uuid/v4');

// RPC over socket.io. could use AJAX just as easily (or moreso), but I'm used to this
const makeLabel = (term, num) => {
	if (!num) {
		return '';
	}
	return num + ' ' + (num === 1 ? term : term + 's');
};

export const getElapsedLabel = (diff) => {
	const descr = {};
	[ [ 'hour', 60*60 ], [ 'minute', 60 ], [ 'second', 1 ] ].forEach((interval) => {
		if (diff >= interval[1]) {
			descr[interval[0]] = Math.floor(diff/interval[1]);
			diff -= descr[interval[0]] * interval[1];
		}
	});

	// only want to see seconds for the first minute, to verify it's ticking
	// otherwise just hours and minutes
	return makeLabel('hour', descr.hour) + ' ' + makeLabel('minute', descr.minute) +
		(descr.minute > 0 || descr.hour > 0 ? '' : ' ' + makeLabel('second', descr.second));
}

export const init = ({ commit }) => {
	// mark which weeks are in the seed data so we know whether we can prompt to load earlier entries
	commit('CHECK_VISIBLE_WEEKS');

	// bind socket.io events
	sock.on('week-list', (weeks) => {
		commit('SET_WEEKS', weeks);
	});
	sock.on('new-task', (task) => {
		commit('LOG', task);
	});
	sock.on('load-tasks', (tasks) => {
		tasks.forEach((task) => {
			commit('LOG', task);
		});
	});
};

export const loadWeek = ({ state }) => {
	sock.emit('get-next-week', state.visibleWeeks.length);
};

let updater;
export const toggleActivity = ({ commit, state }, evt) => {
	if (!state.task) {
		return;
	}
	// log task
	if (state.activity === 'working') {
		const date = new Date;
		sock.emit('log-task', { 'task': state.task, 'tags': state.tags, date, 'time': ((date - state.lastAction)/1000) });
	}
	commit('TOGGLE_ACTIVITY');
	// update timer
	if (updater === undefined) {
		updater = setInterval(() => {
			commit('SET_ELAPSED', getElapsedLabel(Math.round((new Date - state.lastAction)/1000)));
		}, 1000);
	}
};

export const addTag = ({ commit }, evt) => {
	// act only on comma and enter
	if (evt.keyCode !== 13 && evt.keyCode !== 188) {
		return;
	}
	evt.preventDefault();
	const tag = evt.target.value;
	evt.target.value = '';
	commit('ADD_TAG', tag);
};

export const removeTag = ({ commit }, evt) => {
	commit('REMOVE_TAG', evt.target.innerText);
};

export const updateTask = ({ commit }, evt) => {
	commit('UPDATE_TASK', evt.target.value);
};

export const clearTask = ({ commit }) => {
	commit('CLEAR_TASK');
};

