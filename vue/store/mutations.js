'use strict';
/**
 * Stores the app state and the methods that can change it.
 *
 * I picked up CAPITAL_CAMEL_CASE for these methods somewhere to make them stand out in the actions, but it's not that common
 */
export const state = {
	'activity': 'ready',
	'lastAction': undefined,
	'elapsed': undefined,
	'tags': [],
	'task': undefined,
	// seeded from server: first page of log entries and total count
	'log': [],
	'weeks': undefined,
	'visibleWeeks': {}
}

import Vue from 'vue';

export const mutations = {
	TOGGLE_ACTIVITY(state) {
		state.lastAction = new Date;
		state.elapsed = undefined;
		state.activity = state.activity == 'idle' || state.activity == 'ready' ? 'working' : 'idle';
	},
	SET_ELAPSED(state, elapsed) {
		state.elapsed = elapsed;
	},
	SET_WEEKS(state, weeks) {
		state.weeks = weeks;
	},
	ADD_TAG(state, tag) {
		tag = tag.replace(/^\s+|\s+$/g, '');
		if (tag.length && !state.tags.includes(tag)) {
			state.tags.push(tag);
		}
	},
	REMOVE_TAG(state, tag) {
		state.tags = state.tags.filter((t) => { return t !== tag; });
	},
	CLEAR_TASK(state) {
		state.task = '';
	},
	UPDATE_TASK(state, task) {
		state.task = task;
	},
	LOG(state, entry) {
		// it's possible to receive entries out of order, so look at dates to find array pos
		const dt = new Date(entry.date);
		if (!state.log.some((x, idx) => {
			if (new Date(x.date) < dt) {
				state.log.splice(idx, 0, entry);
				return true;
			}
		})) {
			state.log.push(entry);
		}
		if (!state.visibleWeeks.includes(entry.week)) {
			state.visibleWeeks.push(entry.week);
		}
	},
	CHECK_VISIBLE_WEEKS(state) {
		const weeks = {};
		state.log.forEach((entry) => {
			weeks[entry.week] = 1;
		});
		state.visibleWeeks = Object.keys(weeks);
	},
	SEED(state, { key, value }) {
		state[key] = value;
	}
}

