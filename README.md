What's this?
----------------
A simple Vue/Vuex/Express/Redis app for recording time entries. It barely does anything and objectively sucks but I just wanted a big button with a timer to help me keep track of things throughout the day.

Installation
---------------
 * Clone this repo
 * Redis - install a package from your distro, or just download and build it somewhere. It runs fine without installation.
 * JS stuff - Install a recent version of node, which bundles NPM. Run `npm install` to pull in dependencies

Running
-----------
`npm start` runs the server on port 3000.

`node_modules/.bin/webpack -w` runs webpack in "watch mode" for development. Any time you make a change in the `vue/` path it will detect that and build a new bundle to reflect your changes in a web browser.

Usage
--------
Enter a description of the task you're doing. Optionally, also enter some tags. Press comma or enter to delineate them.

Then, use the big button to track your progress. Clicking it toggles between 'working' and 'idle' states. When you go from 'working' to 'idle' it also creates an entry in your work log, rounded up to the next 15 minute interval.

Files
------
 * `package.json` - contains dependencies only for now. Can do some other stuff that becomes important if/when you want to publish as a module. https://docs.npmjs.com/files/package.json
 * `webpack.config.js` - webpack can be instrumented to do a lot of build processes. Here it transpiles all your Vue files into ES2015 so they work in most browsers, and concatenates them into a large cache-friendly bundle. https://webpack.github.io/docs/configuration.html
 * `server.js` - Entry point, invoked by `npm start`. Binds Express and Socket.IO together with some utilities and the middleware needed to support the application. https://expressjs.com/
 * `redis.js` - Redis interface, wraps db queries in promises
 * `add-test-data.js` - Put in junk with a given date passed from the command line, used to verify pagination
 * `public/` - Static files served directly, namely the webpack bundle
 * `views/` - Handlebars templates, used to integrate some parameters into the HTML that are used as seed data for the Vue application (the prior log entries)
 * `vue/`
	 * `main.js` - Gloms together the different parts of your Vue app. Webpack uses this as an entry point to start resolving dependencies to bundle. The purpose of this file is to instantiate Vue with the relevant options, here a custom store provided by Vuex, the target element in the Handlebars template where we want to inject the application, and the component responsible for rendering the root view. (Also includes a polyfill that extends browser support somewhat.)
	 * `components/` - Components all have a template, which defines HTML that is marked up to show how it behaves with respect to data in the store and the methods available in the actions file. So, basically the I/O layer. _Show a list of all X in the store, perhaps, and when one clicks them perform action Y_, e.g. Components usually also have a bit of script that manages dependencies -- pulling in the needed actions, importing any nested components, etc. With Vuex these sections are minimal since the implementation of those things live in the store, and one only needs to connect the method names in the template to the those in the implementation. A style block may also exist, and can have the scoped attribute to automatically apply only to that template.
	 * `store/`
		 * `actions.js` - things that happen according to user events. Clicks, typing in input boxes, submitting forms, etc. Can also contain shared utilities (like here, `getElapsedLabel` is also used in `getters.js`
		 * `getters.js` - computed properties. You are very limited in the types of things you can do to manipulate data in templates, so instead you define functions here that transform the store data into a form that's directly consumable. Here it is used to group the task log into days so that the template has an easy time rendering a heading for each day without repeating it unnecessarily.
		 * `index.js` - pulls together all these files to instantiate Vuex
		 * `mutations.js` - This contains the structure of the data driving the application along with a set of functions that modify it. Action functions are passed a `commit` callback that can invoke these mutators, and any other way of modifying the store results in an error. Consolidating mutability like this is purportedly a good way to manage complexity in large applications.
		 * `plugins.js` - bits of code that modify the way Vue works in general. Vuex is a plugin, and here we have a plugin that takes data attributes from anything rendered by the server with a class name of `vuex-seed` and imports them into the store when the app initializes. So they're non-app-specific modifications to the workflow.

TODO
--------
The app could use a ton of other stuff in principle, but _meh_.

Regarding the build system, one could explore server-side rendering. This is supported and could make the page render with the prior log entries already in place without having to use that plugin to pull in the seed data. It'd be nicer but of limited practical benefit.
