'use strict';
const
	redis = require('redis'),
	bluebird = require('bluebird'),
	moment = require('moment')
	;

// prefer promises to (err, result) callbacks
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
const dbh = redis.createClient();

dbh.persistAsync('task-log');
dbh.persistAsync('tasks-weeks');

const getWeek = (date) => {
	if (typeof(date) === 'string') {
		date = new Date(date);
	}
	let week = moment(date).week();
	if (week < 10) {
		week = '0' + week;
	}
	return { 'key': date.getFullYear() + '-' + week, 'rank': '-' + date.getFullYear() + week, 'time': Math.ceil(date.getTime()) };
};

/**
 * Redis structure:
 *
 * Newest-first sorted set of weeks for whidch there is data:
 * tasks-weeks [ 2017-53, 2017-52, ... ] zset
 *
 * Newest-first sorted set of task entries for each week:
 * task-$WEEK [ json, json, ... ] zset
 */
module.exports = {
	'logTask': (task) => {
		// @TODO remove, allowing client to force date so the test entry script works. should always pick this here
		task.date = task.forceDate ? task.forceDate : new Date;
		const week = getWeek(task.date);
		task.week = week.key;

		// round up to nearest 15m
		const fifteenMinutes = 60 * 15;
		task.time = Math.ceil(task.time);
		if (task.time % fifteenMinutes !== 0) {
			let secsTill = task.time - Math.floor(task.time/fifteenMinutes)*fifteenMinutes;
			task.time += (fifteenMinutes - secsTill);
		}

		return dbh.zaddAsync('tasks-weeks', week.rank, week.key)
			.then(() => {
				return Promise.all([
					dbh.zrangeAsync('tasks-weeks', 0, -1),
					dbh.zaddAsync('tasks-' + week.key, -week.time, JSON.stringify(task)),
					dbh.persistAsync('tasks-' + week.key),
				]);
			})
			.then((res) => {
				return { task, 'weeks': res[0] };
			})

	},
	'getSeedData': () => {
		// first (max) 2 weeks
		return dbh.zrangeAsync('tasks-weeks', 0, 1)
			.then((weeks) => {
				const jobs = weeks.map((weekKey) => {
					// all entries for each week
					return dbh.zrangeAsync('tasks-' + weekKey, 0, -1);
				});
				jobs.push(dbh.zrangeAsync('tasks-weeks', 0, -1));
				return Promise.all(jobs);
			})
			.then((weeks) => {
				const list = weeks.pop();
				return { 'log': [].concat.apply([], weeks).map(JSON.parse), 'weeks': list };
			});
	},
	'getWeek': (after) => {
		return dbh.zrangeAsync('tasks-weeks', after, after)
			.then((key) => {
				return dbh.zrangeAsync('tasks-' + key, 0, -1);
			})
			.then((entries) => {
				return entries.map(JSON.parse);
			})
	},
	'end': (flush = false) => { dbh.end(flush); }
};
