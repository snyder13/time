'use strict';
const
	express = require('express'),
	app = express(),
	http = require('http').Server(app),
	io = require('socket.io')(http),
	hbs = require('express-hbs'),
	redis = require('./redis')
	;

// handlebars templates used to just provide seed data as JSON
hbs.registerHelper('json', (val) => {
	return JSON.stringify(val);
});
app.engine('hbs', hbs.express4()).set('view engine', 'hbs');

// serve static files
app.use(express.static('public'));

// get list of task entries and render to template
app.get('/', (req, res) => {
	//get the last 2 (max) weeks of task logs, and a list of all the weeks in the system
	redis.getSeedData().then(({ log, weeks }) => {
		res.render('index', { 'title': 'Time-keeping', log, weeks });
	});
});

io.on('connection', (sock) => {
	sock.on('log-task', (task) => {
		redis.logTask(task)
			.then(({ task, weeks }) => {
				// broadcast task to all clients
				io.sockets.emit('new-task', task);
				// also the week list, in case this was the first entry for a new week
				io.sockets.emit('week-list', weeks);
			});
	});

	sock.on('get-next-week', (after) => {
		redis.getWeek(after)
			.then((entries) => {
				sock.emit('load-tasks', entries);
			});
	});
});

http.listen(3000);
